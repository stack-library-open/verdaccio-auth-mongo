const mongoose = require('mongoose');
const Auth = require('./lib/auth/hasher');
const User = require('./lib/model/user');

async function init(config) {
    // Setup config
    if(!config.host) config.host = process.env.MONGODB_HOST;
    if(!config.port) config.port = process.env.MONGODB_PORT;
    if(!config.srv) config.srv = process.env.MONGODB_SRV;
    if(!config.username) config.username = process.env.MONGODB_USERNAME;
    if(!config.password) config.password = process.env.MONGODB_PASSWORD;
    if(!config.database) config.database = process.env.MONGODB_DATABASE;

    // Construct URL
    const url = `mongodb${config.srv ? "+srv" : ""}://`
        .concat(`${encodeURIComponent(config.username)}:${encodeURIComponent(config.password)}`)
        .concat(`@${config.host}`)
        .concat(`${config.srv ? "" : ":".concat(config.port)}`)
        .concat(`/${config.database}`);

    // Specify Options
    const options = {
        useNewUrlParser: true,
        useFindAndModify: false,
        socketTimeoutMS: 0,
        keepAlive: true,
        reconnectTries: Number.MAX_SAFE_INTEGER,
        reconnectInterval: 10000
    };

    // Connect to DB
    await mongoose.connect(url, options);
}

// Define Functions
// Authenticate Function
async function authenticate(userId, password, cb) {
    try {
        // Get current Date
        const date = new Date();
        // Fetch User Info
        let user = await User.find(userId);
        // Verify Password
        let newPasswordHash = await Auth.verify(password, user.password);
        // Update Last Auth Try
        await User.updateLastAuthTry(userId, date);
        // Save new Token
        if(newPasswordHash) {
            await User.updatePassword(userId, newPasswordHash, true);
        }
        // Update Last Auth Success
        await User.updateLastAuthSuccess(userId, date);
        // Check if enabled
        if(!user.enabled) {
            throw new Error("User account disabled");
        }
        // Check expiry
        if(user.expiry && date.getTime() >= new Date(user.expiry).getTime()) {
            throw new Error("Account access expired");
        }
        // Init groups is empty
        if(!user.groups) {
            user.groups = [];
            user.groups.push(userId);
        }
        // Done
        return cb(null, user.groups);
    } catch (err) {
        return cb(null, false);
    }
}

// Add User Function
async function adduser(user, password, callback) {
    return callback(null, true);
};

module.exports = function(config, stuff) {
    // Init
    init(config).then(() => {

    }).catch(e => {
        stuff.log
    });

    // Export
    return {
        authenticate: authenticate,
        adduser: adduser
    };
};