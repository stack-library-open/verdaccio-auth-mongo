## Verdaccio-Auth-Mongo

This is a Verdaccio Auth Plugin configured to authenticate users against a MongoDB Database

## Installation

    npm install verdaccio-auth-mongo

## Config

Add this to your `config.yaml`

    auth:
        auth-mongo:
            host: String (ex: localhost)
            port: Number (This will be ignored if 'srv' is true) (ex: 27017)
            username: String (ex: username)
            password: String (ex: password)
            database: String (ex: database)
            srv: Boolean (ex: false) (https://www.mongodb.com/blog/post/mongodb-3-6-here-to-SRV-you-with-easier-replica-set-connections)

## Dependencies
    - mongoose https://www.npmjs.com/package/mongoose
    - secure-password https://www.npmjs.com/package/secure-password