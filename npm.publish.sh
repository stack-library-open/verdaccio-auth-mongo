#!/bin/bash

PKG_NAME=$(node -p "require('./package.json').name")
PKG_VERSION=$(node -p "require('./package.json').version")
REMOTE_PKG_VERSION=$(npm show ${PKG_NAME} version || '')

echo Package: $PKG_NAME -- Local Version: $PKG_VERSION -- Remote Version: $REMOTE_PKG_VERSION

if [ "$PKG_VERSION" != "$REMOTE_PKG_VERSION" ]; then
    echo Updating Package: $PKG_NAME to Version: $PKG_VERSION
    npm publish
else
    echo Remote already has the Latest Version: $REMOTE_PKG_VERSION
fi
