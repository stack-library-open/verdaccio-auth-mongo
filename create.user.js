const mongoose = require('mongoose');
const User = require('./lib/model/user');
const Hasher = require('./lib/auth/hasher');

const DB_URL = process.argv[2];
const USER_TYPE = process.argv[3];
const USERNAME = process.argv[4];
const PASSWORD = process.argv[5];
let GROUPS = [];

GROUPS.push(USERNAME);
if(6 < process.argv.length) {
    GROUPS = GROUPS.concat(process.argv.slice(6, process.argv.length));
}
GROUPS = Array.from(new Set(GROUPS));

mongoose.connect(DB_URL, {
    useNewUrlParser: true
}).then(async () => {
    let user = await User.add(USERNAME, await Hasher.hash(PASSWORD), USER_TYPE, GROUPS, null, true);
    console.log(`Created User\n\nUsertype: ${USER_TYPE}\nUsername: ${user._id}\nPassword: ${PASSWORD}\nGroups: ${JSON.stringify(GROUPS)}\nExpiry: null\nEnabled: true\n`);
    process.exit(0);
}).catch(e => {
    console.error(e);
    process.exit(1);
});