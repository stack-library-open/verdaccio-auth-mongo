const SecPass = require('secure-password');
const hasher = SecPass({
    memlimit: SecPass.MEMLIMIT_DEFAULT,
    opslimit: SecPass.OPSLIMIT_DEFAULT
});

//Hash data
const hashData = function(data) {
    return new Promise((resolve, reject) => {
        hasher.hash(Buffer.from(data), (err, hash) => {
            if(err) {
                reject(err);
            } else {
                resolve(hash);
            }
        });
    });
};

//Verify data
const verifyData = function(data, hash) {
    return new Promise((resolve, reject) => {
        hasher.verify(Buffer.from(data), Buffer.from(hash), async (err, res) => {
            if(err) {
                reject(err);
            } else {
                switch(res) {
                    case SecPass.VALID:
                        resolve();
                        break;
                    case SecPass.VALID_NEEDS_REHASH:
                        try {
                            resolve(await hashData(data));
                        } catch (err) {
                            resolve();
                        }
                        break;
                    default:
                        reject(new Error("Verification failed"));
                }
            }
        });
    });
};

module.exports.hash = hashData;
module.exports.verify = verifyData;
