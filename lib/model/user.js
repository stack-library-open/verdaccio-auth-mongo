const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    // User ID
    _id: {
        type: String
    },
    // User Type
    type: {
        type: String,
        required: true,
        enum: [
            'user',
            'client'
        ],
        default: 'user'
    },
    // Groups
    groups: [String],
    // Hashed Password
    password: {
        type: String,
        required: true
    },
    // Auth Info
    lastAuthTry: {
        type: Date,
        default: null
    },
    lastAuthSuccess: {
        type: Date,
        default: null
    },
    expiry: {
        type: Date,
        default: null
    },
    enabled: {
        type: Boolean,
        required: true,
        default: false
    }
}, {
    strict: true,
    useNestedStrict: true,
    timestamps: true
});

const UserModel = mongoose.model('User', UserSchema);

module.exports.Schema = UserSchema;

module.exports.Model = UserModel;

module.exports.add = function(userId, password, type, groups, expiry, enabled) {
    return new Promise(async function(resolve, reject) {
        try {
            resolve(await UserModel.create({
                _id: userId,
                type: type,
                password: password,
                groups: groups,
                expiry: expiry,
                enabled: enabled
            }));
        } catch (err) {
            reject(err);
        }
    });
};

module.exports.find = function(userId) {
    return new Promise(async function(resolve, reject) {
        try {
            const user = await UserModel.findById(userId).lean();
            if(user) {
                resolve(user);
            } else {
                reject(new Error("User does not exist"));
            }
        } catch (err) {
            reject(err);
        }
    });
};

module.exports.updatePassword = function(userId, password, ignoreFailure) {
    return new Promise(async function(resolve, reject) {
        try {
            await UserModel.findByIdAndUpdate(userId, {
                password: password
            });
            resolve();
        } catch (err) {
            if(ignoreFailure) {
                resolve();
            } else {
                reject(err);
            }
        }
    });
};

module.exports.updateLastAuthTry = function(userId, date) {
    return new Promise(async function(resolve, reject) {
        try {
            await UserModel.findByIdAndUpdate(userId, {
                lastAuthTry: date
            });
            resolve();
        } catch (err) {
            reject(err);
        }
    });
};

module.exports.updateLastAuthSuccess = function(userId, date) {
    return new Promise(async function(resolve, reject) {
        try {
            await UserModel.findByIdAndUpdate(userId, {
                lastAuthSuccess: date
            });
            resolve();
        } catch (err) {
            reject(err);
        }
    });
}

module.exports.delete = function(userId) {
    return new Promise(async function(resolve, reject) {
        try {
            await UserModel.findByIdAndDelete(userId);
            resolve();
        } catch (err) {
            reject(err);
        }
    });
};
